<?php

    require_once('../controller/SliderController.php');
    
    require_once(__DIR__.'/../../core/validador.php');

    $validador = new validador();
    $validador->imgEmpty($_FILES['imagen_url']['name'], 'imagen_url');
    $validador->fileType($_FILES['imagen_url'],'imagen_url', array('gif', 'jpeg', 'jpg', 'png', 'GIF', 'JPEG', 'JPG', 'PNG'));
    
    //$validador -> weightFile($_FILES['imagen']['size'], 'imagen', 'imagen' 2000000); //Un poco menos a 2MB
    // $validador -> fileFormat($_FILES['imagen']['tmp_name'], 'imagen', 'imagen');
    
    //$validador -> widthFile($_FILES['imagen']['width'], 'imagen', 600); //son 600px


    if($validador->getValidador()){
        $slider = new SliderController();
        
        $response['_validador'] = $validador->getValidador();

        if(isset($_POST['id'])){
            $response['message'] = $slider->updateSlider($_POST, $_FILES['imagen_url']); // aqui se pasa los datos de las base de datos del update. se necesita de un if o else para diferenciarlo
        
            echo json_encode($response);
            
        }else{
            $response['message'] = $slider->createSlider($_POST, $_FILES['imagen_url']); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
            
            echo json_encode($response);
        };
    
    }else{

        echo json_encode($validador->getErrors()); //Se imprimen todos los errores
   
    }

?>