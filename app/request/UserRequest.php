<?php
    // require_once ('app/controller/UserController.php');
    require_once ('../controller/UserController.php');
    require_once ('../../core/validador.php');

    $validador = new validador(); 

    $validador -> isEmpty($_POST['name'], 'name');
    $validador -> isEmpty($_POST['lastname'], 'lastname');
    $validador -> isEmpty($_POST['password'], 'password');
    $validador -> isEmpty($_POST['email'], 'email');
    $validador -> isEmpty($_POST['telefono'], 'telefono');
    $validador -> isEmpty($_POST['location'], 'location');
    

    $validador -> isMin($_POST['name'], 'name', 2);
    $validador -> isMax($_POST['name'], 'name', 21);

    $validador -> isMin($_POST['lastname'], 'lastname', 4);
    $validador -> isMax($_POST['lastname'], 'lastname', 42);

    $validador -> isMin($_POST['password'], 'password', 4);
    $validador -> isMax($_POST['password'], 'password', 42);

    $validador -> isMin($_POST['email'], 'email', 5);
    $validador -> isMax($_POST['email'], 'email', 40);

    $validador -> isMin($_POST['telefono'], 'telefono', 9);
    $validador -> isMax($_POST['telefono'], 'telefono', 9);

    $validador -> emailVal($_POST['email'], 'email');


    if($validador->getValidador()){
        $user = new UserController();

        $response['_validador'] = $validador->getValidador();

        if(isset($_POST['id'])){
            $response['message'] = $user->updateUser($_POST); // aqui se pasa los datos de las base de datos del update. se necesita de un if o else para diferenciarlo
        
            echo json_encode($response);

        }else{
            $response['message'] = $user->createUser($_POST); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
            
            echo json_encode($response);
        };
    
    }else{

        echo json_encode($validador->getErrors()); //Se imprimen todos los errores
   
    };
?>