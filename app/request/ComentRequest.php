<?php

    require_once "ComentController.php";
    require_once "validador.php";

    $validador = new validador(); 

    $validador -> isEmpty($_POST['autor'], 'autor');
    $validador -> isEmpty($_POST['comentario'], 'comentario');
    
    $validador -> isMin($_POST['autor'], 'autor', 2);
    $validador -> isMax($_POST['autor'], 'autor', 21);

    $validador -> isMin($_POST['comentario'], 'comentario', 15);
    $validador -> isMax($_POST['comentario'], 'comentario', 500);


    if($validador->getValidador()){
        $coment = new ComentController($_POST);
        $response['_validador'] = $validador->getValidador();
        $response['message'] = $coment->getComent();
        echo json_encode($response);
    
    }else{

    echo json_encode($validador->getErrors()); //Se imprimen todos los errores
   
    }
?>