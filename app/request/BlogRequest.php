<?php
    require_once ('../controller/BlogController.php');
    
    require_once (__DIR__.'/../../core/validador.php');

    $validador = new validador(); 

    $validador -> isEmpty($_POST['title'], 'title');
    $validador -> isEmpty($_POST['category'], 'category');
    $validador -> isEmpty($_POST['articulo'], 'articulo');

    $validador -> isMin($_POST['title'], 'title', 2);
    $validador -> isMax($_POST['title'], 'title', 64);

    $validador -> isMin($_POST['articulo'], 'articulo', 20);
    $validador -> isMax($_POST['articulo'], 'articulo', 2000);

    if($validador->getValidador()){
        $blog = new BlogController();

        $response['_validador'] = $validador->getValidador();
        
        if(isset($_POST['id'])){
            $response['message'] = $blog->updateBlog($_POST); // aqui se pasa los datos de las base de datos del update. se necesita de un if o else para diferenciarlo
        
            echo json_encode($response);
            
        }else{
            $response['message'] = $blog->createBlog($_POST); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
            
            echo json_encode($response);
        };

    }else{

        echo json_encode($validador->getErrors()); //Se imprimen todos los errores
   
    }
?>