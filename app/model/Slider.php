<?php

require_once(__DIR__.'/../../core/Database.php');

class Slider extends Database {

    protected $_imagen_url;
    protected $_title;
    protected $_link;
    protected $_descripcion;

    public function __construct(){

        parent::__construct();
    }

    public function getImagen_url(){

        return $this->_imagen_url;
    }

    public function setImagen_url($imagen_url){

        $this->_imagen = $imagen_url;
    }

    public function getTitle(){

        return $this->_title;
    }

    public function setTitle($title){

        $this->_title = $title;
    }

    public function getLink(){

        return $this->_link;
    }

    public function setLink($link){

        $this->_link = $link;
    }

    public function getDescripcion(){

        return $this->_descripcion;
    }

    public function setDescripcion($descripcion){

        $this->_descripcion = $descripcion;
    }

    public function indexSlider(){//Aplicar a los otros controladores
        $query = "SELECT * 
        FROM `t_slider`";
        $stmt = $this->_pdo->prepare($query); 
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return $result;
    }

    public function showSlider($id){

        $query =  "SELECT * 
        FROM `t_slider` 
        WHERE id = :id"; // :id - Es bind o bindear vendria a significar  dos 'sitios' conectados. Cada columna es un campo. Cada fila es un registro de la tabla. 

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function createSlider($slider, $imagen){

        $query = "insert into t_slider (imagen_url, title, link, descripcion) 
        values (:imagen_url, :title, :link, :descripcion)";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("imagen_url", $imagen);
        $stmt->bindParam("title", $slider['title']);
        $stmt->bindParam("link", $slider['link']);
        $stmt->bindParam("descripcion", $slider['descripcion']);
        $stmt->execute();

        //Aqui se recupera la id del ultimo slider añadido
        $slider_id = $this->_pdo->lastInsertId();

        return "Imagen añadida correctamente con el número de id " . $slider_id;
    }

    public function updateSlider($slider, $imagen){

        $query = "UPDATE t_slider set
            imagen_url = :imagen_url, 
            title = :title, 
            link = :link,
            descripcion = :descripcion
            WHERE id = :id";

        //Aqui se prepara la llamada ala servidor  y la consulta
        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $slider['id']);
        $stmt->bindParam("imagen_url", $imagen);
        $stmt->bindParam("title", $slider['title']);
        $stmt->bindParam("link", $slider['link']);
        $stmt->bindParam("descripcion", $slider['descripcion']);
        $stmt->execute();

        return "Imagen ".$slider['id']." actualizada correctamente";
        
    }

    public function deleteSlider($id){

        $query =  "DELETE 
        FROM `t_slider` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return "El slider con id "+$id+" ha sido añadido";
        
    }
    
}

?>