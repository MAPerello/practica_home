<?php
require_once(__DIR__.'/../../core/Database.php');

class Blog extends Database {

    protected $_title;
    protected $_category;
    protected $_articulo;

    public function __construct(){

        parent::__construct();
    }

    public function getTitle(){

        return $this->_title;
    }

    public function setTitle($title){

        $this->_title = $title;
    }

    public function getCategory(){

        return $this->_category;
    }

    public function setCategory($category){

        $this->_category = $category;
    }

    public function getArticulo(){

        return $this->_articulo;
    }

    public function setArticulo($articulo){

        $this->_articulo = $articulo;
    }

    public function indexBlog(){//Aplicar a los otros controladores
        $query = "SELECT * 
        FROM `t_blog`";
        $stmt = $this->_pdo->prepare($query); 
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return $result;
    }

    public function showBlog($id){

        $query =  "SELECT * 
        FROM `t_blog` 
        WHERE id = :id"; // :id - Es bind o bindear vendria a significar  dos 'sitios' conectados. Cada columna es un campo. Cada fila es un registro de la tabla. 

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function createBlog($blog){
        //insertar en la t_user(tabla user) todos estos parametros
        $query = "insert into t_blog (title, category, articulo) 
        values (:title, :category, :articulo)";

        //Aqui se prepara la llamada ala servidor  y la consulta
        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("title", $blog['title']);
        $stmt->bindParam("category", $blog['category']);
        $stmt->bindParam("articulo", $blog['articulo']);
        $stmt->execute();

        //Aqui se recupera la id del ultimo usuario añadido
        $blog_id = $this->_pdo->lastInsertId();

        return "Articulo añadido correctamente con el número de id " . $blog_id;
    }

    public function updateblog($blog){

        $query = "UPDATE t_blog set
            title = :title, 
            category = :category, 
            articulo = :articulo
            WHERE id = :id";

        //Aqui se prepara la llamada ala servidor  y la consulta
        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $blog['id']);
        $stmt->bindParam("title", $blog['title']);
        $stmt->bindParam("category", $blog['category']);
        $stmt->bindParam("articulo", $blog['articulo']);
        $stmt->execute();

        return "Articulo ".$blog['id']." actualizado correctamente";
        
    }

    public function deleteblog($id){

        $query =  "DELETE 
        FROM `t_blog` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return "El blog con id "+$id+" ha sido añadido";
        
    }

}

?>