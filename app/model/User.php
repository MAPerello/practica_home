<?php

require_once(__DIR__.'/../../core/Database.php');

class User extends Database {

    protected $_name;
    protected $_lastname;
    protected $_password;
    protected $_email;
    protected $_telefono;
    protected $_location;

    public function __construct(){

        parent::__construct();
    }

    public function getName(){

        return $this->_name;
    }

    public function setName($name){

        $this->_name = $name;
    }

    public function getLastname(){

        return $this->_lastname;
    }

    public function setLastname($lastname){

        $this->_lastname = $lastname;
    }

    public function getPassword(){

        return $this->_password;
    }

    public function setPassword($password){

        $this->_password = $password;
    }

    public function getEmail(){

        return $this->_email;
    }

    public function setEmail($email){

        $this->_email = $email;
    }

    public function getTelefono(){

        return $this->_telefono;
    }

    public function setTelefono($telefono){

        $this->_telefono = $telefono;
    }

    public function getLocation(){

        return $this->_location;
    }

    public function setLocation($location){
        
        $this->_location = $location;
    }

    public function indexUser(){//Aplicar a los otros controladores
        $query = "SELECT * 
        FROM `t_user`";
        $stmt = $this->_pdo->prepare($query); 
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return $result;
    }

    public function showUser($id){

        $query =  "SELECT * 
        FROM `t_user` 
        WHERE id = :id"; // :id - Es bind o bindear vendria a significar  dos 'sitios' conectados. Cada columna es un campo. Cada fila es un registro de la tabla. 

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id); // Esta  id se referencia a la id de WHERE. A la consulta le añades el valor $id.
        $stmt->execute();

        /* Array asociativo cada campor de la base de datos aparecera por aqui y tendra un valor
        $result ['name'] */

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function createUser($user){
        //insertar en la t_user(tabla user) todos estos parametros
        $query = "insert into t_user (name, lastname, password, email, telefono, location ) 
        values (:name, :lastname, :password, :email, :telefono, :location )";

        //Aqui se prepara la llamada ala servidor  y la consulta
        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("name", $user['name']);
        $stmt->bindParam("lastname", $user['lastname']);
        $stmt->bindParam("password", $user['password']);
        $stmt->bindParam("email", $user['email']);
        $stmt->bindParam("telefono", $user['telefono']);
        $stmt->bindParam("location", $user['location']);
        $stmt->execute();

        //Aqui se recupera la id del ultimo usuario añadido
        $user_id = $this->_pdo->lastInsertId();

        return "Usuario añadido correctamente con el número de id " . $user_id;
    }

    public function updateUser($user){

        $query = "UPDATE t_user set
            name = :name, 
            lastname = :lastname,
            password = :password, 
            email = :email,  
            telefono = :telefono,
            location = :location
            WHERE id = :id";

        //Aqui se prepara la llamada ala servidor  y la consulta
        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $user['id']);
        $stmt->bindParam("name", $user['name']);
        $stmt->bindParam("lastname", $user['lastname']);
        $stmt->bindParam("password", $user['password']);
        $stmt->bindParam("email", $user['email']);
        $stmt->bindParam("telefono", $user['telefono']);
        $stmt->bindParam("location", $user['location']);
        $stmt->execute();

        return "Usuario ".$user['id']." actualizado correctamente";
        
    }

    public function deleteUser($id){

        $query =  "DELETE 
        FROM `t_user` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return "El usuario con id "+$id+" ha sido añadido";
        
    }

}

?>