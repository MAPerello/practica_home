<?php

require_once "UserController.php";
require_once "BlogController.php";
require_once "SliderController.php";

// Esto unifica las tres llamadas a delete en un solo archivo.

if($_POST['idForm'] == "updateUser"){// "updateUser" va con comillas

    $user = new UserController();
    echo $user->updateUser($_POST);

};

if($_POST['idForm'] == "updateBlog"){// "updateBlog" va con comillas
    
    $blog = new BlogController();
    echo $blog->updateBlog($_POST);

};

if($_POST['idForm'] == "updateSlider"){// "updateSlider" va con comillas

    $slider = new SliderController();
    echo $slider->updateSlider($_POST);

};

//La respuesta se devuelve con echo no con return

/* Se recoge la informacion pasada por $_POST y se extrae 'idForm' como la 'id' 
del formulario desde el que se cliclo el boton "delete" y se usa esa id para 
implementar los condicionales "if($_POST['idForm'] == updateUser o updateBlog o updateSlider)
una vez cumplida la condicion se extra la "id" del boton delete que se cliclo que coincide 
con la "id" de la fila de la base de datos que queremos borrar.*/

?>
