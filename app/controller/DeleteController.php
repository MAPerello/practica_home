<?php

require_once "UserController.php";
require_once "BlogController.php";
require_once "SliderController.php";

// Esto unifica las tres llamadas a delete en un solo archivo.

if($_POST['idForm'] == "listUser"){// "listUser" va con comillas

    $user = new UserController();
    echo $user->deleteUser($_POST['id']);

};

if($_POST['idForm'] == "listBlog"){// "listBlog" va con comillas
    
    $blog = new BlogController();
    echo $blog->deleteBlog($_POST['id']);

};

if($_POST['idForm'] == "listSlider"){// "listSlider" va con comillas

    $slider = new SliderController();
    echo $slider->deleteSlider($_POST['id']);

};

//La respuesta se devuelve con echo no con return

/* Se recoge la informacion pasada por $_POST y se extrae 'idForm' como la 'id' 
del formulario desde el que se cliclo el boton "delete" y se usa esa id para 
implementar los condicionales "if($_POST['idForm'] == listUser o listBlog o listSlider)
una vez cumplida la condicion se extra la "id" del boton delete que se cliclo que coincide 
con la "id" de la fila de la base de datos que queremos borrar.*/

?>
