<?php

require_once (__DIR__.'/../model/User.php');

class UserController {

    protected $_user;

    public function __construct(){
        
        $this->_user = new User();
    }

    public function indexUser(){//Implementar esto en los otros controladores

        return $this->_user->indexUser(); 
    }

    public function showUser($id){

        return $this->_user->showUser($id);
    }

    public function createUser($user){

        $user['password'] = crypt($user['password']); //Encripta la contraseña
        return $this->_user->createUser($user); //Sale por aqui ($user es $_POST)
    }

    public function updateUser($user){

        $user['password'] = crypt($user['password']); //Encripta la contraseña
        return $this->_user->updateUser($user);
    }

    public function deleteUser($id){

        return $this->_user->deleteUser($id);
    }

}

?>
