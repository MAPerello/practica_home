<?php

class ComentController {

    protected $_autor;
    protected $_link;
    protected $_comentario;

    public function __construct($coment){
        $this->_autor = $coment['autor'];
        $this->_link = $coment['link'];
        $this->_comentario = $coment['comentario'];
        
    }

    public function getComent(){

        $coment = $this->_autor . " " . $this->_link . " " . $this->_comentario;

        return $coment;
    }
}

?>