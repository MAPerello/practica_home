<?php

require_once(__DIR__.'/../model/Blog.php');

class BlogController {

    protected $_blog;

    public function __construct(){

        $this->_blog = new Blog();
    }

    public function indexBlog(){//Implementar esto en los otros controladores

        return $this->_blog->indexBlog(); 
    }

    public function showBlog($id){

        return $this->_blog->showBlog($id);
    }

    public function createBlog($blog){

        return $this->_blog->createBlog($blog);
    }

    public function updateBlog($blog){

        return $this->_blog->updateBlog($blog);
    }

    public function deleteBlog($id){

        return $this->_blog->deleteBlog($id);
    }

}

?>