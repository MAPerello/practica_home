<?php

require_once(__DIR__.'/../model/Slider.php');

class SliderController {

    protected $_slider;

    public function __construct(){

        $this->_slider = new Slider();
    }

    public function indexSlider(){//Implementar esto en los otros controladores

        return $this->_slider->indexSlider(); 
    }

    public function showSlider($id){

        return $this->_slider->showSlider($id);
    }

    public function createSlider($slider, $imagen){

        $imagen_url = "practica_home/img/slider/" . basename($imagen['name']); // Aquí se sube la imagen. dirname es el directorio.
        move_uploaded_file($imagen["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $imagen_url);
        
        $imagen_url = "img/slider/" . basename($imagen['name']); // Aqui modificamos la ruta que le pasaremos a la base de datos

        return $this->_slider->createSlider($slider, $imagen_url);
    }

    public function updateSlider($slider, $imagen){

        $imagen_url = "practica_home/img/slider/" . basename($imagen['name']); // Aquí se sube la imagen. dirname es el directorio.
        move_uploaded_file($imagen["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $imagen_url);

        $imagen_url = "img/slider/" . basename($imagen['name']); // Aqui modificamos la ruta que le pasaremos a la base de datos

        return $this->_slider->updateSlider($slider, $imagen_url);
    }

    public function deleteSlider($id){

        return $this->_slider->deleteSlider($id);
    }

}

?>