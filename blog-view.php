<?php include('header-links.php'); ?>

        <!-- /////////////////////////////////////////////////////////////////// -->

    

    <?php 

        require_once("app/Controller/BlogController.php"); 
        $blog_obj = new BlogController(); 
        $blog = $blog_obj->showBlog($_POST['id']);

    ?>

<div>
    <div class="offset-1 col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11" id="contenido">
    <h2>Blog</h2>

    <p><a href="blogs.php">Back</a></p> <!-- Link hacia home -->

        <div id="blog-view" class="blog-view">

            <div class="row">
                    
                <div class="blog col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">

                    <p class="hidden" hidden> <?= $blog['id'];?> </p>

                    <h3 class="title"> <?= $blog['title'];?> </h3>

                    <h5 class="date"> <?= $blog['create_at']; ?> </h5>

                    <h4 class="category"> <?= $blog['category'];?> </h4>

                    <h4 class="cuerpo"> <?= $blog['articulo']; ?> </h4>
                    
                </div>

            </div>

        </div>
    </div>
</div>


<?php include('scriptsContainerClosed.php'); ?>