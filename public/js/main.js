//Dentro "$(document).ready(function()" van las llamadas a librerias
$(document).ready(function() {
    
    // Dentro de la funciona justo aqui
    // Js de insertar un textarea con editor
    // Da conflicto con enviar por consola si se mete dentro del .click
    //Si hay mas de 0 (uno o mas) ckeditor en la pagina, carga ckeditor
    if( $('textarea').length > 0){ 
        CKEDITOR.replace( 'ckeditor' );
    };

    //Selec2

    if( $('.select2').length > 0){
        $('.select2').select2();
    }

});

//Comentarios
//Activa el boton "send"
$(document).on("click","#send",function(event){

    // $("#contacto").validate({ 

    //     ignore: [],
    //     onkeyup: false,
    //     // onfocusout valida el campo cuando el cursor sale de el.
    //     onfocusout: false,
    //     // errorClass permite crear una clase y vincularlar con css.
    //     errorClass: "is-invalid",

    //     rules: {
    //         name: { 
    //             required: true,
    //             minlength: 2,
    //             maxlength: 20,
    //         },
        
    //         email: {
    //             required: true,
    //             email: true,
    //             maxlength: 40,
    //         },
        
    //         telefono: {
    //             required: true,
    //             number: true,
    //             minlength: 9,
    //             maxlength: 9,
    //         },
        
    //         comentario: {
    //             //Intervienen en la validacion del CKEDITOR
    //             required: function()
    //             {
    //             CKEDITOR.instances.comentario.updateElement();  
    //             }
    //         },
            
    //         location: {
    //             required: true,
    //         }
        
    //     },
        
    //     messages: {
    //         name: {
    //             required: "Introduce un nombre",
    //             minlength: "¡¿A eso llamas nombre?!",
    //             maxlength: "Que grande",
    //         },
    //         email: {
    //             required: "Introduce un mail",
    //             email: "No valido",
    //             maxlenght: "Que correo tan grande!"
    //         },
    //         telefono: {
    //             required: "Introduce un numero",
    //             number: "Solo numeros",
    //             minlength: "Minimo 9 digitos",
    //             maxlength: "Maximo 9 digitos",
    //         },
    //         comentario: {
    //             minlength: "Muy corto",
    //         },
    //         location: {
    //             required: "¿Donde estas?"
    //         }
    //     }
    //     })

    if($(".admin-form").valid()){ // Si #contacto es igual a valid, ejecuta lo de abajo

        event.preventDefault();

        var formData =  new FormData($(".admin-form")[0]); // Envia la informacion del formulario
        
        if( $('textarea').length > 0){
            CKEDITOR.instances.ckeditor.updateElement(); 
        };

        var form = "#" + $(".admin-form").attr('id'); // id del formulario
        var url = $(form).prop("action");
        var formData = new FormData($(form)[0]); // id del formulario para comprobar
    
        for(var pair of formData.entries()) {
            console.log(pair[0]+ ', '+ pair[1]);
        };

        var password = $("#password").val();
        var passwordDos = $("#passwordDos").val();
        
        if(password == passwordDos){
    
            $.ajax({
                type:'post',
                url: url,
                dataType:'json', //se espera un json y no un text de texto plano
                data: formData, //Esto es una variable
                cache:false,
                contentType:false,
                processData:false,
                success: function (response) {

                    console.log(response)

                    if(response._validador){
                        
                        $(".errors").html("");//Borrar lista de errores antes de volver a rellenar

                        $(".success").text(response.message);
                        $(".admin-form").fadeOut();
                        $(".success-container").removeClass("hidden").fadeOut().fadeIn(); 

                        //Recargara la pagina despues de tres segundos
                        setTimeout(function(){  location.reload(true); }, 3000);

                    }else{
                    
                        /* Escribir los errores todos juntos encima del formulario*/
        
                        $(".errors-container").removeClass("hidden");

                        $(".errors").html("");//Borrar lista de errores antes de volver a rellenar
        
                        $.each(response._errors, function(i, item) {
                            if(item.message){
                                $(".errors").append("<li>" + item.message + "</li>");
                            }
                        });
                    }
                },

                error: function(response){
                    alert("Esto es un error")
                    console.log(response);
                }

            });
        
        }else{

            $(".errors").html("");
            $(".errors").append("<li> El campo password no coincide, porfavor reviselo </li>");
            return false;
        }

    };

    return false;

});

/* Esta funcion le pasa el valor "id" alojado en .delete para que .eliminat 
pueda ejecutar el borrado */
$(document).on('click', '.delete', function(){

    var id = $(this).attr('id');
    
    $(".eliminar").prop('id', id);
});

//esta es la llamada ajax de borrar
/* Crear un formdata, le pasas a la formdta los valores que quieras 
con .append y lo enviamos por ajax poniendo el dtatype en json */
$(document).on('click', '.eliminar', function(){

    var id = $(this).attr('id');
    var url = $(".admin-table").attr('action');
    var idForm = $(".admin-table").attr('id');

    var formData = new FormData();
    formData.append("id", id);
    formData.append("idForm", idForm);

    $.ajax({
        type: 'POST',
        url: url, //la dirreción al que llamaremos
        dataType: 'json', //es el tipo de dato que espera que le devolvamos
        data: formData,//se almacena los datos
        cache: false,
        contentType: false,
        processData: false,

        success: function (response) {
            
            // alert("Borrado confirmado");
            console.log(response);

            location.reload(true); //Recarga la pagina
            
        },

        error: function (response) {

            alert("Error en borrado")
            console.log(response);

            location.reload(true);
            
        }
    });

    return false;
    
});

/*esta es la llamada ajax de modificar, 
que cargara en el lugar de la lista el formulario de update*/
$(document).on("click",".editar",function(){

    var id = $(this).attr('id');
    var idForm = $(".admin-table").attr('id');
    // var data = {};
    // data['id'] = id;
    var formData = new FormData();
    formData.append("id", id);

    alert(id);
    alert(idForm);
    alert(formData);

    if(idForm=="listUser"){
        // alert("vamos a editar la tabla "+idForm);

        var url = 'update-user.php';

    }else if(idForm=="listBlog"){
        // alert("vamos a editar la tabla "+idForm);

        var url = 'update-blog.php';

    }else if(idForm=="listSlider"){
        // alert("vamos a editar la tabla "+idForm);

        var url = 'update-slider.php';

    }else{
        return false;
    };

    $.ajax({
        type:'post',
         url: url, //se espera un json y no un text de texto plano
         data: formData, //Esto es una variable
         cache:false,
         contentType:false,
         processData:false,
        success: function (response) {

            console.log(response);
            alert("Esto va bien");

            $("#contenido").html(response);

        },

        error: function(response){

            console.log(response);
            alert("Esto saca error");

            $("#contenido").html(response);
        }
    });



    setTimeout(function(){  $(".save").prop('id', id); }, 500);
    /* setTimeout retrasa la ejecucion de $(".save").prop('id', id);
    que al ser mas rapido que .load se requiere esperar medio segundo
    para hacer posible la accion de modificar la id de "save" */

});

//esta es la llamada ajax de salvar
$(document).on("click",".save",function(event){

    event.preventDefault();

    // var id = $(this).attr('id');
    // var url = $(".update-form").attr('action');
    // var idForm = $(".update-form").attr('id');
    // var formText = $(".update-form")[0]; // Envia la informacion del formulario
    
    if( $('textarea').length > 0){
        CKEDITOR.instances.ckeditor.updateElement(); 
    };
    
    var form = "#" + $(".update-form").attr('id'); //id del formulario 
    var url = $(form).prop("action");
    var formData = new FormData($(form)[0]); //id del formulario para comprobar que funciona en la consola

    for (var pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
    };

    var password = $("#password").val();
    var passwordDos = $("#passwordDos").val();
        
    if(password == passwordDos){

        $.ajax({
            type:'post',
            url: url,
            dataType:'json', //se espera un json y no un text de texto plano
            data: formData, //Esto es una variable
            cache:false,
            contentType:false,
            processData:false,
            success: function (response) {

                console.log(response)

                if(response._validador){
                    
                    $(".errors").html(""); //borrara los errores antes de mostrar el mensaje de confirmacion

                    $(".success").text(response.message);
                    $(".update-form").fadeOut();
                    $(".success-container").removeClass("hidden").fadeOut().fadeIn(); 

                    //Recargara la pagina despues de tres segundos
                    setTimeout(function(){  location.reload(true); }, 3000);
                }else{

                    /* Escribir los errores todos juntos encima del formulario*/
                    $(".errors-container").removeClass("hidden");

                    $(".errors").html(""); //borrara los errores antes de volver a mostrarlos.

                    $.each(response._errors, function(i, item) {
                        if(item.message){
                            $(".errors").append("<li>" + item.message + "</li>");
                        }

                    });
                }
            },

            error: function(response){
                console.log(response);
            }

        });
    
    }else{

        $(".errors").html("");
        $(".errors").append("<li> El campo password no coincide, porfavor reviselo </li>");
        return false;
    }
    
    return false;

});

//Carga de un blog en concreto desde seccion Blogs
$(document).on("click",".mas",function(){

    var id = $(this).attr('id');
    //var idForm = $(".blogs-container").attr('id');
    var formData = new FormData();
    formData.append("id", id);

    $.ajax({
        type:'post',
         url: 'blog-view.php', //se espera un json y no un text de texto plano
         data: formData, //Esto es una variable
         cache:false,
         contentType:false,
         processData:false,
        success: function (response) {

            console.log(response);
            alert("Esto va bien");

            $("#contenido").html(response);

        },

        error: function(response){

            console.log(response);
            alert("Esto saca error");

            $("#contenido").html(response);
        }
    });

});

//Carga de un blog en concreto desde seccion Blogs
$(document).on("click",".verMas",function(){

    var id = $(this).attr('id');
    //var idForm = $(".blogs-container").attr('id');
    var formData = new FormData();
    formData.append("id", id);

    $.ajax({
        type:'post',
         url: 'blog-view.php', //se espera un json y no un text de texto plano
         data: formData, //Esto es una variable
         cache:false,
         contentType:false,
         processData:false,
        success: function (response) {

            console.log(response);
            alert("Esto va bien");

            $("#contenido").html(response);

        },

        error: function(response){

            console.log(response);
            alert("Esto saca error");

            $("#contenido").html(response);
        }
    });

});

//AutoRellenado
$(document).on("click","#auto",function(){

    var idForm = $(".admin-form").attr('id');
    var idFormUp = $(".update-form").attr("id");

    // user-form
    if(idForm=="contact-form" || idFormUp=="updateUser"){

        $('#name').val('Niki');

        $('#lastname').val('Lucky Star');

        $('#password').val('1234567890');

        $('#email').val('niki@gmail.com');

        $('#telefono').val('666555444');

        $('#location').val('CDM');

        return false;
    };
    
    // blog-form
    if(idForm=="blog-form" || idFormUp=="updateBlog"){


        $('#title').val('Niki Blog');

        $('#category').val('ROL');

        CKEDITOR.instances.ckeditor.setData('Escribe tus cutres articulos!" Creemos en ti!');

        return false;
    };
    
    // slider-form
    if(idForm=="slider-form" || idFormUp=="updateSlider"){

        $('#imagen').val('');

        $('#title').val('Super Juan Odyssey 3');

        $('#link').val('www.nikolay-blog.com');

        CKEDITOR.instances.ckeditor.setData('Oh si, este juego es lo más en el mundo mundial, dame 3 unidades!');
    
        return false;
    };

    //Mostrara el error en el form
    $(".errors").append("<li> El autocompletado de este formulario no esta disponible </li>");
    return false;

});

// ZONA DE PELIGRO //
// AUTORELLENADO INSANO //


// $(document).ready(function() {

//     var idForm = $(".admin-form").attr('id');
//     var idFormUp = $(".update-form").attr("id");

//     // user-form
//     if(idForm=="contact-form" || idFormUp=="updateUser"){

//         $('#name').val('Niki');

//         $('#lastname').val('Lucky Star');

//         $('#password').val('1234567890');

//         $('#email').val('niki@gmail.com');

//         $('#telefono').val('666555444');

//         $('#location').val('CDM');

    
//     };
    
//     // blog-form
//     if(idForm=="blog-form" || idFormUp=="updateBlog"){


//         $('#title').val('Niki Blog');

//         $('#category').val('ROL');

//         CKEDITOR.instances.ckeditor.setData('Escribe tus cutres articulos!" Creemos en ti!');

    
//     };
    
//     // slider-form
//     if(idForm=="slider-form" || idFormUp=="updateSlider"){

//         $('#imagen').val('');

//         $('#title').val('Super Juan Odyssey 3');

//         $('#link').val('www.nikolay-blog.com');

//         CKEDITOR.instances.ckeditor.setData('Oh si, este juego es lo más en el mundo mundial, dame 3 unidades!');
    
    
//     };

// if($(".admin-form").valid()){ // Si #contacto es igual a valid, ejecuta lo de abajo

    

//     var formData =  new FormData($(".admin-form")[0]); // Envia la informacion del formulario
    
//     if( $('textarea').length > 0){
//         CKEDITOR.instances.ckeditor.updateElement(); 
//     };

//     var form = "#" + $(".admin-form").attr('id'); // id del formulario
//     var url = $(form).prop("action");
//     var formData = new FormData($(form)[0]); // id del formulario para comprobar

//     for(var pair of formData.entries()) {
//         console.log(pair[0]+ ', '+ pair[1]);
//     };

//     $.ajax({
//         type:'post',
//         url: url,
//         dataType:'json', //se espera un json y no un text de texto plano
//         data: formData, //Esto es una variable
//         cache:false,
//         contentType:false,
//         processData:false,
//         success: function (response) {

//             console.log(response)

//             if(response._validador){
                
//                 $(".errors").html("");//Borrar lista de errores antes de volver a rellenar

//                 $(".success").text(response.message);
//                 $(".admin-form").fadeOut();
//                 $(".success-container").removeClass("hidden").fadeOut().fadeIn(); 

//                 //Recargara la pagina despues de tres segundos
//                 setTimeout(function(){  location.reload(true); }, 1000);

//             }else{
            
//                 /* Escribir los errores todos juntos encima del formulario*/

//                 $(".errors-container").removeClass("hidden");

//                 $(".errors").html("");//Borrar lista de errores antes de volver a rellenar

//                 $.each(response._errors, function(i, item) {
//                     if(item.message){
//                         $(".errors").append("<li>" + item.message + "</li>");
//                     }
//                 });
//             }
//         },

//         error: function(response){
//             console.log(response);
//         }

//     });
// };

// return false;

// });