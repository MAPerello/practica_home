<?php include('header.php'); ?>

        <!-- /////////////////////////////////////////////////////////////////// -->

    

    <?php 

        require_once("app/Controller/BlogController.php"); 
        $blog_obj = new BlogController(); 
        $blogs = $blog_obj->indexBlog();

    ?>

<div>
    <div class="offset-1 col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11" id="contenido">
    <h2>Blogs</h2>

    <p><a href="index.php">Home</a></p> <!-- Link hacia home -->

        <div id="blogs-container" class="blogs-container">

            <div class="row">

                <?php foreach($blogs as $blog): ?>
                    
                    <div class="blogs col-5 col-sm-5 col-md-5 col-lg-5 col-xl-5">

                        <p class="hidden" hidden> <?= $blog['id'];?> </p>

                        <h3 class="title"> <?= $blog['title'];?> </h3>

                        <h5 class="date"> <?= $blog['create_at']; ?> </h5>

                        <h4 class="category"> <?= $blog['category'];?> </h4>

                        <h4 class="cuerpo"> <?= $blog['articulo']; ?> </h4>

                        <button id= "<?=$blog['id'];?>" class='mas btn btn-light' type='button' name='mas'>Más</button>
                       
                    </div>

                <?php endforeach ?>

            </div>

        </div>
    </div>
</div>


<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>