<?php //include('header-links.php'); ?>

    <?php 

        require_once('app/controller/UserController.php'); 
        $user_obj = new UserController(); 
        $user = $user_obj->showUser($_POST['id']);

    ?>

    <!-- /////////////////////////////////////////////////////////////////// -->

    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

        <h2>Update User</h2>

        <div class="errors-container hidden">
            <ul class="errors"></ul>
        </div>

        <div class="success-container hidden">
            <h1 class="success"></h1>
        </div>

        <form class="update-form" id="updateUser" action="app/request/UserRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

            <p><a href="list-user.php">Back</a></p> <!-- Link hacia home -->

            <div class="form-group>">

                <input type="text" class="form-control" hidden id="<?= $user['id']; ?>" name="id" value= "<?= $user['id']; ?>"> 

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" id="name" placeholder="Nombre" name="name" value="<?= $user['name']; ?>">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="lastname">Apellidos:</label>
                <input type="text" class="form-control" id="lastname" placeholder="Apellidos" name="lastname" value="<?= $user['lastname']; ?>">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="password">Nuevo Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Contraseña" name="" value="">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="passwordDos">Repita Nuevo Password:</label>
				<input type="password" class="form-control" id="passwordDos" placeholder="Contraseña" name="password" value="">

			</div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="email">Email:</label>
                <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="<?= $user['email']; ?>">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="telefono">Telefono:</label>
                <input type="text" class="form-control" id="telefono" placeholder="Telefono" name="telefono" value="<?= $user['telefono']; ?>">

            </div>

            <div class="form-group">
                <!-- Selec2 -->
                <label for="location">Location:</label>
                <select class="select2" name="location" id="location" value="<?= $user['location']; ?>">
                    <option value=""></option>
                    <option value="CDM">Ciudadano del Mundo</option>
                    <option value="AL">Alabama</option>
                    <option value="ALA">Alacama</option>
                    <option value="WA">Washington</option>
                    <option value="WY">Wyoming</option>

                </select>

            </div>

            <button id="" type="button" class="save btn btn-info" name="save" value="save">Save</button>
            
            <button id="auto" type="button" class="btn btn-danger">Auto</button>

        </form>

    </div>

        
<?php //include('scriptsContainerClosed.php'); ?>