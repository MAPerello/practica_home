<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<!-- Link carpeta estilos CSS -->
	<link rel="stylesheet" type="text/css" href="public/css/style.css">
	<!-- Link Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Hind+Guntur" rel="stylesheet">

	<title>Práctica Home</title>

</head>
    
<body>
    
    <div class="container-fluid">
        <!-- Cabezera -->
        <div class="row"> <!--Esto lo convierte en fila-->

            <!-- Logo -->
            <div class="logo col-6 col-sm-6 col-md-9 col-lg-9 col-xl-9">
            
            <a href="index.php" title="IndexNintendoPage"> 
                <img src="img/logo-nintendo.png" />
            </a>
            
            </div>
                
            <!-- Menu -->
            <div class="menu-top col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">

                <ul class="navbar-nav">

                    
                    <a href="list-user.php">list-user</a>
                
                
                    <a href="list-blog.php">list-blog</a>
                
                
                    <a href="list-slider.php">list-slider</a>
                

                </ul>

            </div>
                
        </div>