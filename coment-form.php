<?php include('header.php'); ?>

<div class="form-box">

	<!-- Formulario -->
	<div class="formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		
		<h2>Comentario</h2>

		<div class="errors-container hidden">

    		<ul class="errors"></ul>

		</div>

		<div class="success-container hidden">

			<h1 class="success"></h1>

		</div>

		<form class="admin-form" id="coment-form" action="app/request/ComentRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="Autor">Autor:</label>
				<input type="text" class="form-control" id="autor" placeholder="Autor" name="autor">

				<p id="autor-error" class="input-error"></p>

			</div>

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="Link">Link:</label>
				<input type="text" class="form-control" id="link" placeholder="Link" name="link">

				<p id="link-error" class="input-error"></p>
            
            </div>

            <div class="form-group">
				<label for="comentario">Escriba su comentario:</label>
				
				<textarea class="form-control" name="comentario" rows="3" id="ckeditor" placeholder="Escriba su comentario"></textarea> 
			
                <p id="comentario-error" class="input-error"></p>

            </div>

			

			<button id="send" type="submit" class="btn btn-primary">Enviar</button>
		
		</form>

		

		
	
	</div>

</div>
        
<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>