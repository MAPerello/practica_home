<?php include('header.php'); ?>

<div class="form-box">

	<!-- Formulario -->
	<div class="formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		
		<h2>Formulario</h2>

		<div class="errors-container hidden">

    		<ul class="errors"></ul>

		</div>

		<div class="success-container hidden">

			<h1 class="success"></h1>

		</div>

		<form class="admin-form" id="contact-form" action="app/request/UserRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

			

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="nombre">Nombre:</label>
				<input type="text" class="form-control" id="name" placeholder="Nombre" name="name">

				<p id="name-error" class="input-error"></p>

			</div>

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="Apellido">Apellido:</label>
				<input type="text" class="form-control" id="lastname" placeholder="Apellido" name="lastname">

				<p id="lastname-error" class="input-error"></p>

			</div>

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="password">Password:</label>
				<input type="password" class="form-control" id="password" placeholder="Contraseña" name="password">

				<p id="password-error" class="input-error"></p>

			</div>

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="passwordDos">Repita Password:</label>
				<input type="password" class="form-control" id="passwordDos" placeholder="Contraseña" name="">

				<p id="passwordDos-error" class="input-error"></p>

			</div>

			<div class="form-group">
				<label for="email">Email:</label>
				<!-- Si se coloca type="email" saltara una validacion desde html -->
				<input type="text" class="form-control" id="email" placeholder="Email" name="email">
			
				<p id="email-error" class="input-error"></p>
			
			</div>

			<div class="form-group">
				<label for="telefono">Telefono:</label>
				<input type="text" class="form-control" id="telefono" placeholder="Telefono" name="telefono">
			
				<p id="telefono-error" class="input-error"></p>

			</div>
			
			<div class="form-group">
				<!-- Selec2 -->
				<label for="location">Location:</label>
				<select class="select2" name="location" id="location">
					<option value=""></option>
					<option value="CDM">Ciudadano del Mundo</option>
					<option value="AL">Alabama</option>
					<option value="ALA">Alacama</option>
					<option value="WA">Washington</option>
					<option value="WY">Wyoming</option>

				</select>

				<p id="location-error" class="input-error"></p>

			</div>

			<button id="send" type="submit" class="btn btn-primary">Enviar</button>

			<button id="auto" type="button" class="btn btn-danger">Auto</button>
		
		</form>

		<p id="respond"></p>

		
	
	</div>

</div>
        
<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>