<?php include('header-links.php'); ?>

	<?php 

		require_once('app/controller/BlogController.php'); 
		$blog_obj = new BlogController(); 
		$blog = $blog_obj->showBlog($_POST['id']);

	?>

	<!-- /////////////////////////////////////////////////////////////////// -->

	<div class="formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		
		<h2>Update Blog</h2>

		<div class="errors-container hidden">
            <ul class="errors"></ul>
        </div>

        <div class="success-container hidden">
            <h1 class="success"></h1>
        </div>

		<form class="update-form" id="updateBlog" action="app/request/BlogRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

			<p><a href="list-blog.php">Back</a></p> <!-- Link hacia home -->

			<div class="form-group>">

				<input type="text" class="form-control" hidden id="<?= $blog['id']; ?>" name="id" value= "<?= $blog['id']; ?>"> 

			</div>

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="title">Titulo:</label>
				<input type="text" class="form-control" id="title" placeholder="Titulo" name="title" value="<?= $blog['title']; ?>">

			</div>
			
			<div class="form-group">
				<!-- Selec2 -->
				<label for="category">Categoria:</label>
				<select class="select2" name="category" id="category" value="<?= $blog['category']; ?>">
					
					<option value="">Selecciona Categoria</option>
					<option value="ACC">Acción</option>
                    <option value="LUC">Lucha</option>
					<option value="ARC">Arcade</option>
					<option value="PLA">Plataformas</option>
					<option value="DIS">Disparos</option>
					<option value="EST">Estrategia</option>
					<option value="SIM">Simulación</option>
                    <option value="DE">Deporte</option>
					<option value="CAR">Carreras</option>
					<option value="AVE">Aventura</option>
                    <option value="AVE-CON">Aventura conversacional</option>
					<option value="AVE-GRA">Aventura gráfica</option>
                    <option value="ACC-AVE">Acción-aventura</option>
					<option value="SUR-HOR">Survival horror</option>
					<option value="SIG">Sigilo</option>
					<option value="ROL">Rol</option>
                    <option value="SAN">Sandbox</option>
					<option value="MUS">Musical</option>
					<option value="PUZ">Puzzle</option>
                    <option value="PAR">Party games</option>
					<option value="EDU">Educación</option>

				</select>

			</div>

			<div class="form-group">
				<label for="articulo">Modifique su articulo:</label>
				<textarea class="form-control" name="articulo" rows="3" id="ckeditor" placeholder="Escriba su articulo" value= "<?= $blog['articulo']; ?>"></textarea>

			</div>

			<button id="" type="button" class="save btn btn-info" name="save" value="Save">Save</button>
		
			<button id="auto" type="button" class="btn btn-danger">Auto</button>

		</form>

	</div>

<?php include('scriptsContainerClosed.php'); ?>