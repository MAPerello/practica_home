<?php include('header.php'); ?>

        <!-- /////////////////////////////////////////////////////////////////// -->

    

    <?php 

        require_once("app/Controller/BlogController.php"); 
        $blog_obj = new BlogController(); 
        $blogs = $blog_obj->indexBlog();

    ?>

<div>
    <div class="offset-1 col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11" id="contenido">
    <h2>Blogs</h2>

    <p><a href="index.php">Home</a></p> <!-- Link hacia home -->

        <div id="blogs-container" class="blogs-container">

            <div class="row">

                <?php foreach($blogs as $blog): ?>
                    
                    <div class="blogs col-11 col-sm-5 col-md-3 col-lg-2 col-xl-2">

                        <p class="hidden" hidden> <?= $blog['id'];?> </p>

                        <h3 class="title"> <?= $blog['title'];?> </h3>

                        <h5 class="date"> <?= $blog['create_at']; ?> </h5>

                        <h4 class="category"> <?= $blog['category'];?> </h4>

                        <button id= "<?=$blog['id'];?>" class='mas btn btn-light' type='button' name='mas'>Ver más</button>
                       
                    </div>

                <?php endforeach ?>

            </div>

            <div class="row">

                <div class="blogs-list col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">
                    
                    <select value="" id="blogsList" class="blogsList" name="blogsList" multiple>
                        
                        <?php foreach($blogs as $blog): ?>

                            <option class="verMas" id= "<?=$blog['id'];?>"> <?= $blog['title'];?> - <?= $blog['category'];?></option>

                        <?php endforeach ?>

                    </select>
                
                </div>

            </div>

        </div>
    </div>
</div>


<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>