        <!-- Destacados -->
        <!-- Articulos -->
                    
        <div class="articulos col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h2>World News</h2>

            <div class="row">

                <div class="articulo col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
                    <img src="img/logo2.jpg" />
                    <div class="text">
                        <h4>Monster Hunter XX (3DS)</h4>
                        <p>
                            Descripción: Monster Hunter XX para Nintendo Switch y 3DS amplía y mejora los contenidos de Monster Hunter Generations, 
                            introduciendo nuevas criaturas a las que dar caza, más áreas por explorar, dos nuevos estilos de combate y nuevas variantes 
                            de Diablos. El videojuego de Capcom Monster Hunter XX no se olvida tampoco de las armas y el equipo de batalla, que se amplía 
                            con nuevos ítems.
                        </p>
                    </div>

                    <div class="button-box">
                        <a target="blank" href="https://www.playstation.com/es-es/" title="PlayStation"> <!-- target="blank" Abre el link en pestaña nueva -->
                            <button type="button" class="btn btn-light">Information
                            </button> <!-- botones de bootstrap https://getbootstrap.com/docs/4.0/components/buttons/ -->
                        </a>
                    </div>
                </div>

                <div class="articulo col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
                    <img src="img/logo3.png" />
                    <div class="text">
                        <h4>Nintendo Switch</h4>
                        <p>
                            Descripción: Una consola de sobremesa y a la vez también portátil. Esta es la apuesta de la Gran N con Nintendo Switch, 
                            su nueva videoconsola, anteriormente conocida con el nombre en clave NX. ¿Cómo definirla? Es una máquina híbrida centrada 
                            en el juego tradicional en casa, pero capacitada para llevar toda esa experiencia a una pantalla más pequeña para disfrutar 
                            de los videojuegos dónde, cuándo y con quién quieras, igual que una consola portátil.
                        </p>
                    </div>

                    <div class="button-box">
                        <a target="blank" href="https://www.playstation.com/es-es/" title="PlayStation"> <!-- target="blank" Abre el link en pestaña nueva -->
                            <button type="button" class="btn btn-light">Information
                            </button> <!-- botones de bootstrap https://getbootstrap.com/docs/4.0/components/buttons/ -->
                        </a>
                    </div>
                </div>

                <div class="articulo col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
                    <img src="img/logo4.jpg" />
                    <div class="text">
                        <h4>Radiant Historia - Perfect Chronology (3DS)</h4>
                        <p>
                            Descripción: Radiant Historia: Perfect Chronology es un videojuego de Atlus para Nintendo 3DS que recupera
                            para los nuevos tiempos y para 3DS un clásico del año 2011 que apareció en la videoconsola portátil DS. 
                            Con redibujado del arte de los personajes, nuevo doblaje y algunas partes de la historia inéditas, 
                            Radiant Historia: Perfect Chronology ofrece dos formas de afrontarlo. 
                        </p>
                    </div>

                    <div class="button-box">
                        <a target="blank" href="https://www.playstation.com/es-es/" title="PlayStation"> <!-- target="blank" Abre el link en pestaña nueva -->
                            <button type="button" class="btn btn-light">Information
                            </button> <!-- botones de bootstrap https://getbootstrap.com/docs/4.0/components/buttons/ -->
                        </a>
                    </div>
                </div>

            </div>
            
        </div>