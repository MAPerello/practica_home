<?php include('header.php'); ?>

<div class="form-box">

	<!-- Formulario -->
	<div class="formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		
		<h2>Crear Entrada Blog</h2>

		<div class="errors-container hidden">

    		<ul class="errors"></ul>

		</div>

		<div class="success-container hidden">

			<h1 class="success"></h1>

		</div>

		<form class="admin-form" id="blog-form" action="app/request/BlogRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

			

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="title">Titulo:</label>
				<input type="text" class="form-control" id="title" placeholder="Titulo" name="title">

				<p id="title-error" class="input-error"></p>

			</div>
			
			<div class="form-group">
				<!-- Selec2 -->
				<label for="category">Categoria:</label>
				<select class="select2" name="category" id="category">
					
					<option value="">Selecciona Categoria</option>
					<option value="ACC">Acción</option>
                    <option value="LUC">Lucha</option>
					<option value="ARC">Arcade</option>
					<option value="PLA">Plataformas</option>
					<option value="DIS">Disparos</option>
					<option value="EST">Estrategia</option>
					<option value="SIM">Simulación</option>
                    <option value="DE">Deporte</option>
					<option value="CAR">Carreras</option>
					<option value="AVE">Aventura</option>
                    <option value="AVE-CON">Aventura conversacional</option>
					<option value="AVE-GRA">Aventura gráfica</option>
                    <option value="ACC-AVE">Acción-aventura</option>
					<option value="SUR-HOR">Survival horror</option>
					<option value="SIG">Sigilo</option>
					<option value="ROL">Rol</option>
                    <option value="SAN">Sandbox</option>
					<option value="MUS">Musical</option>
					<option value="PUZ">Puzzle</option>
                    <option value="PAR">Party games</option>
					<option value="EDU">Educación</option>

				</select>

				<p id="class-error" class="input-error"></p>

			</div>

			<div class="form-group">
				<label for="articulo">Escriba su articulo:</label>
				<!-- Debe ir dentro de "<form>" -->
				<!-- Inserta un textarea con editor -->
				<textarea class="form-control" name="articulo" rows="3" id="ckeditor" placeholder="Escriba su articulo"></textarea> 
			</div>

			<p id="articulo-error" class="input-error"></p>

			<button id="send" type="submit" class="btn btn-primary">Enviar</button>

			<button id="auto" type="button" class="btn btn-danger">Auto</button>
		
		</form>

	</div>

</div>
        
<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>