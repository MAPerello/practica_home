<?php include('header.php'); ?>

<div class="form-box">

	<!-- Formulario -->
	<div class="formulario col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		
		<h2>Slider Formulario</h2>

		<div class="errors-container hidden">

    		<ul class="errors"></ul>

		</div>

		<div class="success-container hidden">

			<h1 class="success"></h1>

		</div>

		<form class="admin-form" id="slider-form" action="app/request/SliderRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

			

			<div class="form-group"> <!-- Agrupara el label con el input -->
				<label for="imagen">Imagen:</label>
				<input type="file" class="form-control" id="imagen" placeholder="Imagen" name="imagen_url">

				<p id="imagen-error" class="input-error"></p>

			</div>

			<div class="form-group">
				<label for="title">Titulo:</label>
				<!-- Si se coloca type="email" saltara una validacion desde html -->
				<input type="text" class="form-control" id="title" placeholder="Titulo" name="title">
			
				<p id="title-error" class="input-error"></p>
			
			</div>

			<div class="form-group">
				<label for="link">Enlace:</label>
				<input type="text" class="form-control" id="link" placeholder="Enlace" name="link">
			
				<p id="link-error" class="input-error"></p>

			</div>

			<div class="form-group">
				<label for="descripcion">Escriba su descripcion:</label>
				<!-- Debe ir dentro de "<form>" -->
				<!-- Inserta un textarea con editor -->
				<textarea class="form-control" name="descripcion" rows="3" id="ckeditor" placeholder="Escriba su descripcion"></textarea> 
			</div>

			<p id="descripcion-error" class="input-error"></p>

			<button id="send" type="submit" class="btn btn-primary">Enviar</button>

			<button id="auto" type="button" class="btn btn-danger">Auto</button>
		
		</form>

	</div>

</div>
        
<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>