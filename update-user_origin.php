<?php include('header-links.php'); ?>

    <!-- /////////////////////////////////////////////////////////////////// -->

    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

        <h2>Update User</h2>

        <form class="update-form" id="updateUser" action="app/controller/UpdateController.php"> <!-- La id se coloca al form (Formulario), no al div -->

            <p><a href="list-user.php">Back</a></p> <!-- Link hacia home -->

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" id="name" placeholder="Nombre" name="name">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="lastname">Apellidos:</label>
                <input type="text" class="form-control" id="lastname" placeholder="Apellidos" name="lastname">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Password" name="password">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="email">Email:</label>
                <input type="text" class="form-control" id="email" placeholder="Email" name="email">

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="telefono">Telefono:</label>
                <input type="text" class="form-control" id="telefono" placeholder="Telefono" name="telefono">

            </div>

            <div class="form-group">
                <!-- Selec2 -->
                <label for="location">Location:</label>
                <select class="select2" name="location" id="location">
                    <option value=""></option>
                    <option value="CDM">Ciudadano del Mundo</option>
                    <option value="AL">Alabama</option>
                    <option value="ALA">Alacama</option>
                    <option value="WA">Washington</option>
                    <option value="WY">Wyoming</option>

                </select>

            </div>

            <button id="" type="button" class="save btn btn-info" name="save" value="Save">Save</button>
            
            <button id="auto" type="button" class="btn btn-danger">Auto</button>

        </form>

    </div>

        
<?php include('scriptsContainerClosed.php'); ?>