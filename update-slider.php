<?php include('header-links.php'); ?>

    <?php 

        require_once('app/controller/SliderController.php'); 
        $slider_obj = new SliderController(); 
        $slider = $slider_obj->showSlider($_POST['id']);

    ?>

    <!-- /////////////////////////////////////////////////////////////////// -->

    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

        <h2>Update Slider</h2>

        <div class="errors-container hidden">
            <ul class="errors"></ul>
        </div>

        <div class="success-container hidden">
            <h1 class="success"></h1>
        </div>

        <form class="update-form" id="updateSlider" action="app/request/SliderRequest.php"> <!-- La id se coloca al form (Formulario), no al div -->

            <p><a href="list-slider.php">Back</a></p> <!-- Link hacia home -->

            <div class="form-group>">

                <input type="text" class="form-control" hidden id="<?= $slider['id']; ?>" name="id" value= "<?= $slider['id']; ?>"> 

            </div>

            <div class="form-group"> <!-- Agrupara el label con el input -->
                <label for="imagen">Imagen:</label>
                <input type="file" class="form-control" id="imagen" placeholder="Imagen" name="imagen_url" value="<?= $slider['imagen_url']; ?>">

            </div>

            <div class="form-group">
                <label for="title">Titulo:</label>
                <input type="text" class="form-control" id="title" placeholder="Titulo" name="title" value="<?= $slider['title']; ?>">
        
            </div>

            <div class="form-group">
                <label for="link">Enlace:</label>
                <input type="text" class="form-control" id="link" placeholder="Enlace" name="link" value="<?= $slider['link']; ?>">
        
            </div>

            <div class="form-group">
                <label for="descripcion">Escriba su descripcion:</label>
                <textarea class="form-control" name="descripcion" rows="3" id="ckeditor" placeholder="Escriba su descripcion" value= "<?= $slider['descripcion']; ?>" ></textarea> 
        
            </div>

            <button id="" type="button" class="save btn btn-info" name="save" value="Save">Save</button>
            
            <button id="auto" type="button" class="btn btn-danger">Auto</button>

        </form>

    </div>

        
<?php include('scriptsContainerClosed.php'); ?>