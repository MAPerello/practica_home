<?php

    // require_once "UserController.php";
    // require_once "BlogController.php";

    class validador {

        protected $_validador;
        protected $_errors;

        /* Hemos creado la funcion de isEmpty, es decir, está vacío(?)
        no especificamos el label ya q podemos simplificar poniendo esto porque recogerá
        cada valor segun lo que escriban en los inputs y cada label.*/
        public function __construct(){
            $this->_validador = true;
            $this->_errors = array();
        }

        public function getValidador(){
            return $this->_validador;
        }

        // devuelve todas las variables de este objeto 
        public function getErrors(){
            return get_object_vars($this);
        }
        
        //--------------------------------------------

        public function isEmpty($value, $label){ //Destinado a cualquier campo vacio
        
            if(empty($value)){
                
                array_push($this->_errors, array(
                    'message' => "Complete el campo vacio " . $label,
                    'id' => $label
                ));

                $this->_validador = false;
            }
        }

        public function isMin($value, $label, $min_length){ //Da el minimo de caracteres

            if(strlen($value) < $min_length) {

                array_push($this->_errors, array(
                    'message' => "El campo " . $label . " debe contener minimo" . $min_length . " caracteres",
                    'id' => $label
                ));

                $this->_validador = false;
            }
        }

        public function isMax($value, $label, $max_length){ //Da el maximo de caracteres

            if(strlen($value) > $max_length) {

                array_push($this->_errors, array(
                    'message' => "El campo " . $label . " debe contener maximo " . $max_length . " caracteres",
                    'id' => $label
                ));

                $this->_validador = false;
            }
        }

        public function emailVal($value, $label){ //Destinado a email

            if(!filter_var($value, FILTER_VALIDATE_EMAIL)){

                array_push($this->_errors, array(
                    'message' => "El campo " . $label . " es invalido",
                    'id' => $label
                ));

                $this->_validador = false;
            }
        }

        //Valida si hay algun archivo para ser evaluado/cargado
        public function imgEmpty($value, $input_label){   
            if(empty($value)){               
                array_push($this->_errors, array(
                    'message' => "no has selecionado ninguna imagen",
                    'id' => 'imagen'
                ));

                $this->_validador = false;
            }

        }

        //Valida si erchivo tiene la extension requerida
        public function fileType($file, $input_label, $allowed_extensions){

            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

            if(!in_array($extension, $allowed_extensions)){  

                array_push($this->_errors, array(
                    'message' => "el formato no es valido",
                    'id' => "imagen"
                ));

                $this->_validador = false;
            }

        }

//         public function imgFormat($value, $label){
//        if (!file_exists('#imagenSlider')) {
//            array_push($this->_errors, array(
//                "message" => "No has seleccionado ninguna imagen",
//                'id' => 'imagen',
//            ));

//            $this->_validation = false;
//        }
//    }  

        // public function weightFile($label, $value, $weight){ //Destinado a img
            
        //     //Valida si el archivo intruducido sobrepasa el peso/tamaño en bytes designado
        //     if(filesize($value) > $weight) { 

        //         array_push($this->_errors, array(
        //             'message' => "El campo " . $label . " solo permite archivos menores a " . $weight . " bytes",
        //             'id' => $label
        //         ));

        //         $this->_validador = false;
        //     }
        // }

        // public function widthFile($label, $value, $width){ //Destinado a img
            
        //     //Valida si el archivo intruducido sobrepasa el ancho designado
        //     if(getimagesize($value) > $width) { 

        //         array_push($this->_errors, array(
        //             'message' => "El campo " . $label . " solo permite archivos menores a " . $width . " pixels",
        //             'id' => $label
        //         ));

        //         $this->_validador = false;
        //     }
        // }

    }



?>