<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<!-- Link carpeta estilos CSS -->
	<link rel="stylesheet" type="text/css" href="public/css/style.css">
	<!-- Link Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Hind+Guntur" rel="stylesheet">

	<title>Práctica Home</title>

</head>
    
<body>
    
    <div class="container-fluid">
        <!-- Cabezera -->
        <div class="row"> <!--Esto lo convierte en fila-->

            <!-- Logo -->
            <div class="logo col-6 col-sm-6 col-md-9 col-lg-9 col-xl-9">
                
                <a href="index.php" title="IndexNintendoPage"> 
                    <img src="img/logo-nintendo.png" />
                </a>
                
            </div>
                
            <!-- Menu -->
            <div class="menu-top col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                
                <div class="menu">
                
                    <!-- Boostrap 4 -->
                    <!-- Menu Desplegable -->
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" 
                            data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" 
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">

                                <li class="nav-item">
                                    <a class="nav-link" href="blogs.php">Blogs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="sliders.php">Galeria</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://www.google.es/">Login</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="user-form.php">Register</a>
                                </li>

                            </ul>
                        </div>
                    </nav>
                    
                </div>
            </div>

                </div>
            </div>
            
        </div>