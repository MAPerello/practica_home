<?php include('header.php'); ?>

        <!-- /////////////////////////////////////////////////////////////////// -->

    

    <?php 

        require_once('App/controller/sliderController.php'); 
        $slider_obj = new SliderController(); 
        $sliders = $slider_obj->indexSlider();

    ?>

<div>
    <div class="offset-1 col-11 col-sm-11 col-md-11 col-lg-11 col-xl-11" id="contenido">
    <h2>Galeria</h2>

    <p><a href="index.php">Home</a></p> <!-- Link hacia home -->

        <div id="sliders-container" class="sliders-container">

            <div class="row">

                <?php foreach($sliders as $slider): ?>
                    
                    <div class="sliders col-5 col-sm-5 col-md-3 col-lg-2 col-xl-2">

                        <img class="imagenUrl" id="<?=$slider['id'];?>" src="<?= $slider['imagen_url']; ?>" title="<?= $slider['title']; ?>">
                       
                    </div>

                <?php endforeach ?>

            </div>

        </div>
    </div>
</div>


<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>