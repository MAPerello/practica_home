        <!-- Slider -->
        <div class="slider row"> <!--Esto lo convierte en fila-->
            
            <!-- Imagen Slider -->
            <!-- Carousel -->
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="img/logo5-0.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img src="img/logo5-1.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img src="img/logo5-2.png" alt="Third slide">
                        </div>
                    </div>
                </div>
                
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            
            </div>
            
            <!-- Texto Slider -->
            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                <h3>"Switch and Play"</h3>
                <h4>
                    The Nintendo Switch's advertising campaign involved the slogan "Switch and Play" 
                    to show the versatility of playing the console anywhere. Alternatively, 
                    the slogan "Play anywhere, anytime, with anyone" has been used in various European trailers 
                    featuring the console.
                </h4>

                <button type="button" class="btn btn-light">
                    More Information
                </button>
            </div>
            
            <div style="margin-top:1em"></div>
            
            
        </div>