<?php include('header-admin.php'); ?>

        <!-- /////////////////////////////////////////////////////////////////// -->

    

    <?php 

        require_once("app/Controller/BlogController.php"); 
        $blog_obj = new BlogController(); 
        $blogs = $blog_obj->indexBlog();

    ?>

<div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="contenido">
    <h2>List Blog</h2>

    <p><a href="index.php">Home</a></p> <!-- Link hacia home -->

        <form id="listBlog" method="post" class="admin-table" 
            action="app/controller/DeleteController.php">

            <table class="table table-striped">

                <thead>

                    <tr>
                        <th>ID</th>
                        <th>TITULO</th>
                        <th>CATEGORIA</th>
                        <th>ARTICULO</th>
                        <th>FECHA CREACION</th>
                        <th>ELIMINAR</th>
                        <th>MODIFICAR</th>
                    </tr>

                    <?php foreach($blogs as $blog): ?>

                    <tr>
                        <td><?= $blog['id']; ?></td>
                        <td><?= $blog['title']; ?></td>
                        <td><?= $blog['category']; ?></td>
                        <td><?= $blog['articulo']; ?></td> 
                        <td><?= $blog['create_at']; ?></td>

                        <td>
                            <!-- Button trigger modal -->
                            <button id= "<?=$blog['id'];?>" type="button" class="delete btn btn-danger" data-toggle="modal" data-target="#exampleModal" value="delete">Delete</button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Atención:</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                            Esta seguro que quiere eliminar esta fila de la base de datos?
                                        </div>

                                        <div class="modal-footer">
                                            
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button id= "" class='eliminar btn btn-primary' type='button' name='eliminar' value='eliminar'>Eliminar</button></td>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>

                        <td><button id= "<?=$blog['id'];?>" class='editar btn btn-info' type='button' name='editar' value='editar'>Editar</button></td>
                        
                    </tr>

                    <?php endforeach ?>

                </thead>
            </table>
        </form>
    </div>
</div>


<?php include('footer.php'); ?>
<?php include('scripts.php'); ?>